#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

#define TABLE_IMPLEMENTATION
#include "table.h"

#define TABLE_CLASS_NAME Entity
#define TABLE_CLASS_FIELDS\
	X(color, uint32_t)\
	X(x_vel, float)\
	X(y_vel, float)\
	X(x_pos, float)\
	X(y_pos, float)
#include "table.h"

typedef uint32_t u32;
typedef float f32;

uint16_t spawnEntity(struct table *table, struct Entity* entity, u32 entity_color, float x, float y) {
	uint16_t entity_id = table_add_object(table);

	*table_get_object_field(table, entity_id, entity->color, u32) = entity_color;
	*table_get_object_field(table, entity_id, entity->x_vel, f32) = 0.0;
	*table_get_object_field(table, entity_id, entity->y_vel, f32) = 0.0;
	*table_get_object_field(table, entity_id, entity->x_pos, f32) = x;
	*table_get_object_field(table, entity_id, entity->y_pos, f32) = y;

	return entity_id;
}

void debugEntities(struct table* table, struct Entity* entity) {
	u32* color = table_get_field(table, entity->color, u32);
	f32* x_vel = table_get_field(table, entity->x_vel, f32);
	f32* y_vel = table_get_field(table, entity->y_vel, f32);
	f32* x_pos = table_get_field(table, entity->x_pos, f32);
	f32* y_pos = table_get_field(table, entity->y_pos, f32);

	for (int i = 0; i < table->objs_len; i++) {
		printf("id: %d\n", i);
		printf("color: %08x\n", color[i]);
		printf("x_vel: %f\n", x_vel[i]);
		printf("y_vel: %f\n", y_vel[i]);
		printf("x_pos: %f\n", x_pos[i]);
		printf("y_pos: %f\n", y_pos[i]);
	}
}

void render(struct table* table, struct Entity* entity) {
	u32* color = table_get_field(table, entity->color, u32);
	f32* x_pos = table_get_field(table, entity->x_pos, f32);
	f32* y_pos = table_get_field(table, entity->y_pos, f32);
	for (int i = 0; i < table->objs_len; i++) {
		DrawRectangle(
			x_pos[i] - 25,
			y_pos[i] - 25,
			50,
			50,
			*(Color*)&color[i]
		);
	}
}

void update(struct table* table, struct Entity* entity, uint16_t jugador_id) {
	f32* x_vel = table_get_field(table, entity->x_vel, f32);
	f32* y_vel = table_get_field(table, entity->y_vel, f32);
	f32* x_pos = table_get_field(table, entity->x_pos, f32);
	f32* y_pos = table_get_field(table, entity->y_pos, f32);

	float dx = 0, dy = 0;

	if (IsKeyDown(KEY_W)) dy = -1;
	if (IsKeyDown(KEY_A)) dx = -1;
	if (IsKeyDown(KEY_S)) dy = +1;
	if (IsKeyDown(KEY_D)) dx = +1;

	x_vel[jugador_id] = dx;
	y_vel[jugador_id] = dy;

	float dt = GetFrameTime() * 120;
	for (int i = 0; i < table->objs_len; i++) {
		x_pos[i] += x_vel[i] * dt;
		y_pos[i] += y_vel[i] * dt;

		if (i == jugador_id) continue;

		float jx = x_pos[jugador_id];
		float jy = y_pos[jugador_id];
		float ex = x_pos[i];
		float ey = y_pos[i];

		float x = jx - ex;
		float y = jy - ey;
		float l = sqrt(x*x+y*y);

		if (l > 100) {
			x_vel[i] = x / l;
			y_vel[i] = y / l;
		} else {
			x_vel[i] = 0;
			y_vel[i] = 0;
		}
	}
}

float ranpos() {
	return (float)rand() / (float)RAND_MAX * 500.0;
}

int main(void) {
	srand(time(NULL));

	struct table table = {0};

	struct Entity entity = {0};
	tableAppendClassEntity(&table, &entity);

	uint16_t jugador_id = spawnEntity(&table, &entity, 0xffff0000, 250, 250);
	for (int i = 0; i < 20; i++)
		spawnEntity(&table, &entity, 0xff0000ff, ranpos(), ranpos());

	debugEntities(&table, &entity);

	InitWindow(500, 500, "unjuego");

	SetTargetFPS(60);

	while (!WindowShouldClose()) {
		update(&table, &entity, jugador_id);

		BeginDrawing();
			ClearBackground(LIGHTGRAY);
			render(&table, &entity);
		EndDrawing();
	}

	table_destroy(&table);
}
