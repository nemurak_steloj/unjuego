#ifndef TABLE_HEADER
#define TABLE_HEADER

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct state {
	uint16_t
		next,
		prev;
	uint32_t
		generation: 31;
	bool
		active;
};

struct table {
	uint8_t*
		ptr;
	uint16_t
		flds_len,
		flds_cap,
		objs_len,
		objs_cap;
};

struct field {
	uint16_t objs;
	uint16_t size;
};

uint16_t table_object_size(struct table* self);
void table_destroy(struct table* self);
uint16_t table_add_field(struct table* self, uint16_t field_size);
uint16_t table_add_object(struct table* self);
struct field* table_get_raw_field(struct table* self, uint16_t field_id);
void* table_get_raw_object_field(struct table* self, uint16_t object_id, uint16_t field_id);

#define table_get_field(self, field_id, type) ((type*)table_get_raw_object_field(self, 0, field_id))
#define table_get_object_field(self, object_id, field_id, type)\
	((type*)table_get_raw_object_field(self, object_id, field_id))

#define TABLE_CAT(a, b) a ## b
#define TABLE_CATE(a, b) TABLE_CAT(a, b)

#endif // TABLE_HEADER

#ifdef TABLE_IMPLEMENTATION
#undef TABLE_IMPLEMENTATION

void reverse_memcpy(void* dst, void* src, size_t len) {
	uint8_t *lasts = (uint8_t*)src + (len-1);
	uint8_t *lastd = (uint8_t*)dst + (len-1);
	while (len--) {
		*lastd-- = *lasts--;
	}
}

uint16_t table_object_size(struct table* self) {
	uint16_t object_size = 0;
	for (int i = 0; i < self->flds_len; i++) {
		object_size += ((struct field*)self->ptr)[i].size;
	}

	return object_size;
}

uint8_t* table_old_field_case_flds(struct table* self, uint16_t object_offset) {
	return self->ptr + self->flds_len*sizeof(struct field) + self->objs_cap*object_offset;
}


uint8_t* table_old_field_case_objs(struct table* self, uint16_t object_offset) {
	return self->ptr + self->flds_cap*sizeof(struct field) + self->objs_len*object_offset;
}

void table_resize(struct table* self, uint8_t*(*old_field_ptr)(struct table*, uint16_t)) {
	printf("table_resize at fields %d and object %d\n", self->flds_len, self->objs_len);
	uint16_t object_size = table_object_size(self);

	self->ptr = realloc(self->ptr, self->flds_cap*sizeof(struct field) + self->objs_cap*object_size);

	if (!self->ptr) {
		printf("¡¡¡ES EL FIN DEL MUNDO!!! AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n");
		exit(-1);
	}

	struct field* field = (struct field*)self->ptr;
	uint16_t object_offset = object_size;

	for (int f = self->flds_len-1; f >= 0; f--) {
		object_offset -= field[f].size;

		field[f].objs = self->objs_cap*object_offset;

		uint8_t* old = old_field_ptr(self, object_offset);;
		uint8_t* new = self->ptr + self->flds_cap*sizeof(struct field) + self->objs_cap*object_offset;

		reverse_memcpy(new, old, self->objs_len*field[f].size);
	}

	// en caso de que los datos del primer campo sean sobreescritos por los datos de los campos usar esta aproximacion
	// nota: remover la linea -10
	//for (int f = 0; f < self->flds_len; f++) {
	//	field[f].objs = self->objs_cap*object_offset;
	//	object_offset += field[f].size;
	//}
}

void table_destroy(struct table* self) {
	free(self->ptr);
	*self = (struct table) {0};
}

uint16_t table_add_field(struct table* self, uint16_t field_size) {
	if (self->flds_len == self->flds_cap) {
		self->flds_cap = self->flds_len*3/2 + 4;
		table_resize(self, table_old_field_case_flds);
	}

	uint16_t field_id = self->flds_len++;

	((struct field*)self->ptr)[field_id].size = field_size;

	return field_id;
}

uint16_t table_add_object(struct table* self) {
	if (self->objs_len == self->objs_cap) {
		self->objs_cap = self->objs_len*3/2 + 4;
		table_resize(self, table_old_field_case_objs);
	}

	return self->objs_len++;
}

struct field* table_get_raw_field(struct table* self, uint16_t field_id) {
	return &((struct field*)self->ptr)[field_id];
}

void* table_get_raw_object_field(struct table* self, uint16_t object_id, uint16_t field_id) {
	struct field field = *table_get_raw_field(self, field_id);;
	return self->ptr + self->flds_cap*sizeof(struct field) + field.objs + object_id*field.size;
}

#endif // TABLE_IMPLEMENTATION


#if defined(TABLE_CLASS_NAME) && defined(TABLE_CLASS_FIELDS)

#define X(name, type) uint16_t name;
struct TABLE_CLASS_NAME {
	TABLE_CLASS_FIELDS
};
#undef X

#define X(name, type) class->name = table_add_field(self, sizeof(type));
void TABLE_CATE(tableAppendClass, TABLE_CLASS_NAME) (struct table* self, struct TABLE_CLASS_NAME* class) {
	TABLE_CLASS_FIELDS
}
#undef X

#undef TABLE_NAME
#undef TABLE_FIELDS

#endif
