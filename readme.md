# UnJuego
Una prueba de un concepto que vi por ahí en internet sobre programacion orientada a datos.
Lo que hago es crear un grupo de colecciones de datos donde
almaceno y accedo cada campo del objeto.

Un simple ejemplo del codigo:
```c
struct table table = {0};

uint16_t x_pos_id = table_add_field(&table, sizeof(float));
uint16_t y_pos_id = table_add_field(&table, sizeof(float));

uint16_t jugador_id = table_add_object(&table);
*table_get_object_field(&table, jugador_id, x_pos_id, float) = 10;
*table_get_object_field(&table, jugador_id, x_pos_id, float) = 20;

uint16_t enemigo_id = table_add_object(&table);
*table_get_object_field(&table, enemigo_id, x_pos_id, float) = 0;
*table_get_object_field(&table, enemigo_id, x_pos_id, float) = 0;

table_destroy(&table);
```
