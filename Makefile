PKG_CONFIG ?= pkg-config
CCFLAGS=-std=c2x -g `pkg-config --cflags raylib`
LDFLAGS=-lm `pkg-config --libs raylib`

all: unj

run: unj
	./unj

test: unj
	gdb ./unj

unj: main.o
	$(CC) main.o $(CCFLAGS) $(LDFLAGS) -o $@

main.o: main.c table.h
	$(CC) $(CCFLAGS) -c main.c

.PHONY: clean
clean:
	$(RM) ./unj ./*.o
