with import <nixpkgs> {};
mkShell {
	nativeBuildInputs = with buildPackages; [
		gdb
		gnumake
		raylib
		pkg-config
	];
}
